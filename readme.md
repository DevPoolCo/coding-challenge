## BlackBoard Coding Challenge
Here's what I was able to complete for the coding challenge.  

In short, I imported all of the Profile data from the legacy system into my own MongoAtlas DB.  Then I wrote my API to 
call the MongoAtlas DB.  I was not able to complete the authentication process in time.  

However, my goal was to take 
the facebook token from the legacy API, and verify it by making a call to the legacy API with the facebook token.  If I 
received a status 200, then I was going to generate my own access token, and require that in the header as 
Authorization: Bearer {{new access token}}

The API Documentation can be found here: 
https://documenter.getpostman.com/view/7251960/S1Zxc9tb?version=latest

GitLab Repo - https://gitlab.com/DevPoolCo/coding-challenge

I setup this up on a server as well, but I'm hitting a few roadblocks with the DNS propagating properly.
the server is going to be at https://herorank.devpool.co



## Skeleton for Node.js applications written in TypeScript

### Development

```bash
npm run dev
```

### Running tests

```bash
npm test
```

### Linting

```bash
npm run lint
```

### Building a container

```bash
docker build .
```
