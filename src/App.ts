/* eslint-disable */
import * as express from 'express';
const characterController = require('./controllers/character.controller')();
// const authController = require('./controllers/auth.controller')();

  class App {
    public express

    constructor() {
      this.express = express()
      this.mountRoutes()
    }

    private mountRoutes(): void {
      const router = express.Router()
    router.get('/', (req, res) => {
        res.json({
          message: 'Hello World!'
        })
      });

      router.get('/heroes', characterController.heroes);
      router.get('/villains', characterController.villains);
      router.get('/compare/:hero/:villain', characterController.goodVSbad);
      router.get('/profile/:id', characterController.agentID);
      router.get('/profile/:id/:node', characterController.agentNode);
      router.get('/search/name/:q', characterController.searchName);

      // I used this to import all of the characters into my new MongoDB
      // router.get('/import', characterController.agentImport);
      this.express.use('/', router)
    }
  }
export default new App().express
