/* eslint-disable */

// import * as mongoServices from "../services/mongo.service";
// import * as legacyServices from "../services/legacy.service";


const CharacterController = () => {

    const MongoClient = require('mongodb').MongoClient;
    let db
    const uri = 'mongodb+srv://thejason:draAxufIjDTRubj7@cluster0-ybjhz.mongodb.net/test?retryWrites=true&w=majority'


    // Returns all Heroes
    const heroes = async (req, res) => {
        const collectName = 'characters';
        const query = {"biography.alignment" : "good"};
        MongoClient.connect(uri, async (err, client) => {
            if (err) return console.log(err)
            db = client.db('HeroRank')
            db.collection(collectName).find(query,{fields:{id:1,name:1}}).toArray((err, results) => {
                if (err) { res.status(500).json({msg:"There was an error during your request."})}
                res.status(200).json(results);
            })
        })
    };

    // Returns all Villains
    const villains = async (req, res) => {
    const collectName = 'characters';
    const query = {"biography.alignment" : "bad"};
    MongoClient.connect(uri, async (err, client) => {
        if (err) return console.log(err)
        db = client.db('HeroRank')
        db.collection(collectName).find(query,{fields:{id:1,name:1}}).toArray((err, results) => {
            if (err) { res.status(500).json({msg:"There was an error during your request."})}
            res.status(200).json(results);
        })
    })
};

    // Compare 1-Villain vs 1-Hero
    const goodVSbad = async (req, res) => {
        const collectName = 'characters';
        const query1 = {"id" : req.params.hero};
        const query2 = {"id" : req.params.villain};
        const projection = {};


        MongoClient.connect(uri, async (err, client) => {
            if (err) return console.log(err)
            db = client.db('HeroRank')

            //  const getHero = await (findCharacter(collectName, query1, projection));
            // const getVillain = await (findCharacter(collectName, query2, projection));
            db.collection(collectName).find(query1).toArray((err, results) => {
                if (err) { res.status(500).json({msg:"There was an error during your request."})}
                // res.status(200).json(results);
                const getHero = results;

                db.collection(collectName).find(query2).toArray((err, results) => {
                    if (err) { res.status(500).json({msg:"There was an error during your request."})}
                    const getVillain = results

                    if(getHero && getVillain) {
                        let heroScore = 0;
                        let villainScore = 0;

                        if(Number(getHero[0].powerstats.intelligence) > Number(getVillain[0].powerstats.intelligence)) {heroScore = heroScore + 1}
                        if(Number(getVillain[0].powerstats.intelligence) > Number(getHero[0].powerstats.intelligence)) {villainScore = villainScore + 1}
                        if(Number(getHero[0].powerstats.strength) > Number(getVillain[0].powerstats.strength)) {heroScore = heroScore + 1}
                        if(Number(getVillain[0].powerstats.strength) > Number(getHero[0].powerstats.strength)) {villainScore = villainScore + 1}
                        if(Number(getHero[0].powerstats.speed) > Number(getVillain[0].powerstats.speed)) {heroScore = heroScore + 1}
                        if(Number(getVillain[0].powerstats.speed) > Number(getHero[0].powerstats.speed)) {villainScore = villainScore + 1}
                        if(Number(getHero[0].powerstats.durability) > Number(getVillain[0].powerstats.durability)) {heroScore = heroScore + 1}
                        if(Number(getVillain[0].powerstats.durability) > Number(getHero[0].powerstats.durability)) {villainScore = villainScore + 1}
                        if(Number(getHero[0].powerstats.power) > Number(getVillain[0].powerstats.power)) {heroScore = heroScore + 1}
                        if(Number(getVillain[0].powerstats.power) > Number(getHero[0].powerstats.power)) {villainScore = villainScore + 1}
                        if(Number(getHero[0].powerstats.combat) > Number(getVillain[0].powerstats.combat)) {heroScore = heroScore + 1}
                        if(Number(getVillain[0].powerstats.combat) > Number(getHero[0].powerstats.combat)) {villainScore = villainScore + 1}
                        let winner = '';
                        if(heroScore > villainScore) {winner = 'Hero'}
                        if(villainScore > heroScore) {winner = 'Villain'}
                        if(heroScore == villainScore) {winner = 'Draw'}
                        return res.status(200).json({hero:getHero[0].name,heroStats:getHero[0].powerstats,villain:getVillain[0].name,villainStats:getVillain[0].powerstats,winner:winner,heroScore:heroScore,villainScore:villainScore});
                    } else {
                        return res.status(500).json({msg:"no heroes found"});
                    }

                });

            });
        })

    };


    // Search Name only
    const searchName = async (req, res) => {
        const collectName = 'characters';
        const query = {name : {$regex:`.*${req.params.q}*.`}};
        console.log(query);
        MongoClient.connect(uri, async (err, client) => {
            if (err) return console.log(err)
            db = client.db('HeroRank')
            db.collection(collectName).find(query).toArray((err, results) => {
                if (err) { res.status(500).json({msg:"There was an error during your request."})}
                res.status(200).json(results);
            })
        })
    };

    // Return Entire Character by ID
    const agentID = async (req, res) => {
        const collectName = 'characters';
        const query = {"id" : req.params.id};
        MongoClient.connect(uri, async (err, client) => {
            if (err) return console.log(err)
            db = client.db('HeroRank')
            db.collection(collectName).find(query).toArray((err, results) => {
                if (err) { res.status(500).json({msg:"There was an error during your request."})}
                res.status(200).json(results);
            })
        })
    };

    // Return Only Power Stats for Character by ID
    const agentNode = async (req, res) => {
        const collectName = 'characters';
        const query = {"id" : req.params.id};
        MongoClient.connect(uri, async (err, client) => {
            if (err) return console.log(err)
            db = client.db('HeroRank')
            db.collection(collectName).find(query,{fields:{id:1,name:1, [req.params.node]:1}}).toArray((err, results) => {
                if (err) { res.status(500).json({msg:"There was an error during your request."})}
                res.status(200).json(results);
            })
        })
    };


    // Imports new character profiles from legacy to new DB  (Only imports up to 25-records at a time)
    // This was just used for a 1-time import of Character data from the legacy DB to the new DB
    /*
    const agentImport = async (req, res) => {

        var i;
        for (i = 701; i < 731; i++) {
            const myURL = 'https://superheroapi.com/api/10156223285190913/' + i;
            axios.all([axios.get(myURL)]).then(
                axios.spread((res1) => {
                    console.log(res1.data);
                    legacyServices.addChar(db,res1.data);
                })
            ).catch(error => {
                console.log("error")
                console.log(error);
            });
        }
        res.statusCode = 200;
        return res;
    };
    */

    /* If I had enough time I would have added the features below to break down characters by groups and race. */
    // /groups
    // /group/:name
    // /races
    // /race/:type

    return {
        heroes,
        villains,
        goodVSbad,
        searchName,
        agentID,
        agentNode
    }
    // removed agentImport after data import was complete.
}

module.exports = CharacterController;
